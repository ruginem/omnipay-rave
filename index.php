<?php

	require "vendor/autoload.php";

	use Omnipay\Omnipay;
	use Omnipay\Rave\Gateway;

    $gateway = Omnipay::create('Rave');

    $data = [
    	'secretApiKey' => 'FLWSECK-e07912a4650d39ab7c184776816e0f34-X',
		'publicApiKey' => 'FLWPUBK-5133c5838d41ab491aabfabf40348af2-X',
        'transactionId' => 'RAVE_TEST_OMNIPAY_'.date('Ymdhis').'_'.time(),
        'payment_options' => 'card,account,ussd,qr,mpesa,mobilemoneyghana',
        'description' => 'Test Payment via omnipay ',
        'amount' => 510,
        'currency' => 'UGX',
        'country' => 'UG',
        'meta' => 'this is a test',
        'customer_firstname' => 'Gilbert',
        'customer_lastname' => 'Rutatiina',
        'customer_email' => 'rutatiina@ruginem.org',
        'customer_phone' => '0701030399',
        'custom_title' => 'Mixa Publishing',
        //'redirect_url' => 'http://ruginem.com/complete1',
        'returnUrl' => 'http://ruginem.com/complete2',
    ];


	try {

		$purchase = $gateway->purchase($data)->send();

	} catch (Exception $e) {
		echo $e->getMessage();
		exit;
	}

	if ($purchase->isRedirect()) {
		//var_dump($purchase->getRedirectUrl()); exit;
		//var_dump($purchase->getRedirectData()); exit;
		$purchase->redirect();
	} elseif ($purchase->isSuccessful()) {

		echo 'isSuccessful';
		exit;
	}

	echo 'end of code';

	/*

	$data = [
		'transactionId' => $order->id,
		'amount' => $order->total->round()->amount(),
		'currency' => setting('default_currency'),
		'localeCode' => $order->locale,
		'items' => $this->items(),
		'cancelUrl' => $this->getCancelUrl($order),
		'returnUrl' => $this->getReturnUrl($order),
	];


	try {

		$this->gateway()->completePurchase($data)->send();

	} catch (Exception $e) {
		echo $e->getMessage();
	}

	*/