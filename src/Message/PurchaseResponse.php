<?php

namespace Omnipay\Rave\Message;

use Omnipay\Common\Message\AbstractResponse;
use Omnipay\Common\Message\RequestInterface;
use Omnipay\Common\Message\RedirectResponseInterface;

/**
 * Response
 */
class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        $this->request = $request;
        $this->data = $data;
    }

    public function isSuccessful()
    {
        return (isset($this->data['status']) && $this->data['status'] == 'success');
    }

    public function isRedirect()
    {
        return (isset($this->data['status']) && $this->data['status'] != 'error');
    }

    public function getRedirectUrl()
    {
        return $this->data['data']['link'];
    }

    public function getRedirectMethod()
    {
        return 'GET';
    }

    public function getRedirectData()
    {
        return null;
    }

    public function getTransactionReference()
    {
        if (isset($this->data['txref'])) {
            return $this->data['txref'];
        }
    }

}
