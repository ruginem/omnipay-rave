<?php

namespace Omnipay\Rave\Message;

use Omnipay\Common\Message\AbstractRequest;

class CompletePurchaseRequest extends AbstractRequest
{
    public function getData()
    {
        $this->validate('txref');
    }

    public function sendData($data)
    {
        $url = $this->getEndpoint().'?'.http_build_query($data, '', '&');
        $response = $this->httpClient->get($url);

        $data = json_decode($response->getBody(), true);

        return $this->createResponse($data);
    }

    public function getEndpoint()
    {
        return parent::getEndpoint() . '/charges/' . $this->getTransactionReference();
    }
}
