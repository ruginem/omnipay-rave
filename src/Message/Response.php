<?php

namespace Omnipay\Rave\Message;

use Omnipay\Common\Message\AbstractResponse;
use Omnipay\Common\Message\RequestInterface;

/**
 * Response
 */
class Response extends AbstractResponse
{
    public function __construct(RequestInterface $request, $data)
    {
        $this->request = $request;
        $this->data = $data;
    }

    public function isSuccessful()
    {
        return (isset($this->data['status']) && $this->data['status'] == 'successful' && isset($this->data['chargecode']) && $this->data['chargecode'] == '00' );
    }

    public function getTransactionReference()
    {
        if (isset($this->data['txref'])) {
            return $this->data['txref'];
        }
    }

}
