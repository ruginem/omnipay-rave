<?php

namespace Omnipay\Rave\Message;
use GuzzleHttp\Middleware;

use Omnipay\Common\Message\AbstractRequest as BaseAbstractRequest;

/**
 * Abstract Request
 *
 */
class PurchaseRequest extends BaseAbstractRequest
{
    //protected $liveEndpoint = 'https://api.ravepay.co';
    //protected $testEndpoint = 'https://ravesandboxapi.flutterwave.com';
    protected $liveEndpoint = 'https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/hosted/pay';
    protected $testEndpoint = 'https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/hosted/pay';

    public function getData()
    {
    	//$this->validate('amount', 'currency');

		//print_r($this->getParameters()); exit;

		$data = [];

		//$data['secretApiKey'] = $this->getParameter('secretApiKey');
		$data['txref'] = $this->getParameter('transactionId');
		$data['PBFPubKey'] = $this->getParameter('publicApiKey');
		$data['payment_options'] = $this->getParameter('payment_options');
		$data['description'] = $this->getParameter('description');
		$data['amount'] = $this->getParameter('amount');
		$data['currency'] = $this->getParameter('currency');
		$data['country'] = $this->getParameter('country');
		$data['meta'] = $this->getParameter('meta');
		$data['customer_firstname'] = $this->getParameter('customer_firstname');
		$data['customer_lastname'] = $this->getParameter('customer_lastname');
		$data['customer_email'] = $this->getParameter('customer_email');
		$data['customer_phone'] = $this->getParameter('customer_phone');
		$data['custom_title'] = $this->getParameter('custom_title');
		$data['redirect_url'] = $this->getParameter('returnUrl');

        return $data;
    }

    public function getSecretApiKey()
    {
        return $this->getParameter('secretApiKey');
    }

    public function setSecretApiKey($value)
    {
        return $this->setParameter('secretApiKey', $value);
    }

    public function getPublicApiKey()
    {
        return $this->getParameter('publicApiKey');
    }

    public function setPublicApiKey($value)
    {
        return $this->setParameter('publicApiKey', $value);
    }

    //***********************************************************************************************

    public function setTxref($value)
    {
        return $this->setParameter('txref', $value);
    }

    public function setPaymentOptions($value)
    {
        return $this->setParameter('payment_options', $value);
    }

    public function setCountry($value)
    {
        return $this->setParameter('country', $value);
    }

    public function setMeta($value)
    {
        return $this->setParameter('meta', $value);
    }

    public function setCustomerFirstname($value)
    {
        return $this->setParameter('customer_firstname', $value);
    }

    public function setCustomerLastname($value)
    {
        return $this->setParameter('customer_lastname', $value);
    }

    public function setCustomerEmail($value)
    {
        return $this->setParameter('customer_email', $value);
    }

    public function setCustomerPhone($value)
    {
        return $this->setParameter('customer_phone', $value);
    }

    public function setCustomTitle($value)
    {
        return $this->setParameter('custom_title', $value);
    }

    public function setRedirectUrl($value)
    {
        return $this->setParameter('redirect_url', $value);
    }

    //***********************************************************************************************

    public function sendData($data)
    {
    	//print_r($data); exit;
        //$url = $this->getEndpoint().'?'.http_build_query($data, '', '&');

		$options = json_encode($data); //print_r($options); exit;

		/*
        $request = $this->httpClient->createRequest(
        	'POST',
			$this->getEndpoint(),
			['content-type: application/json'],
			$options
		);

        $response = $this->httpClient->sendRequest($request);

        $data = json_decode($response->getBody(), true);
		print_r($data); exit;
		*/

		$ch = curl_init($this->getEndpoint());
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $options);

		// Set HTTP Header for POST request
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($options))
		);

		$result = curl_exec($ch);
		//var_dump($result); exit;

		$data = json_decode($result, true);
		//print_r($data); exit;

		return $this->response = new PurchaseResponse($this, $data);
        //return $this->createResponse($data);
    }

    protected function getBaseData()
    {
        return [
            'transaction_id' => $this->getTransactionId(),
            'expire_date' => $this->getCard()->getExpiryDate('mY'),
            'start_date' => $this->getCard()->getStartDate('mY'),
        ];
    }

    protected function getEndpoint()
    {
        return $this->getTestMode() ? $this->testEndpoint : $this->liveEndpoint;
    }

    protected function createResponse($data)
    {
        return $this->response = new Response($this, $data);
    }
}
