<?php

namespace Omnipay\Rave;

use Omnipay\Common\AbstractGateway;

/**
 * Rave Gateway
 */
class Gateway extends AbstractGateway
{
    public function getName()
    {
        return 'Rave';
    }

    public function getDefaultParameters()
    {
        return [
            'secretApiKey' => '',
            'publicApiKey' => '',
            'testMode' => false,

			'txref' => '',
			'payment_options' => 'card,account,ussd,qr,mpesa,mobilemoneyghana',
			'description' => '',
			'amount' => 0,
			'currency' => 'UGX',
			'country' => 'UG',
			'meta' => '',
			'customer_firstname' => '',
			'customer_lastname' => '',
			'customer_email' => '',
			'customer_phone' => '',
			'custom_title' => '',
			'redirect_url' => '',
        ];
    }

    /*
    public function getSecretApiKey()
    {
        return $this->getParameter('secretApiKey');
    }

    public function setSecretApiKey($value)
    {
        return $this->setParameter('secretApiKey', $value);
    }

    public function getPublicApiKey()
    {
        return $this->getParameter('publicApiKey');
    }

    public function setPublicApiKey($value)
    {
        return $this->setParameter('publicApiKey', $value);
    }
    */

    // this function is for CHARGE
    public function purchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Rave\Message\PurchaseRequest', $parameters);
    }

    //this function is for VERIFY
    public function completePurchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Rave\Message\CompletePurchaseRequest', $parameters);
    }
}
