<?php

namespace PHPSTORM_META {

    /** @noinspection PhpIllegalArrayKeyTypeInspection */
    /** @noinspection PhpUnusedLocalVariableInspection */
    $STATIC_METHOD_TYPES = [
      \Omnipay\Omnipay::create('') => [
        'Rave' instanceof \Omnipay\Rave\RaveGateway,
      ],
      \Omnipay\Common\GatewayFactory::create('') => [
        'Rave' instanceof \Omnipay\Rave\RaveGateway,
      ],
    ];
}
